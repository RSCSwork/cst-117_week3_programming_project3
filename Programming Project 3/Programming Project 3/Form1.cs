﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Project_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void OpenFileBtn_Click(object sender, EventArgs e)
        {
            //check if a file was opened.
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                String readText;
                String[] currentWord;
                String firstWordByAlphabet = "";
                String lastWordByAlphabet = ""; 
                String longestWord = "";
                String wordWithMostVowels = "";
                char[] wordCharArray;
                int currentVowelCount;
                int mostVowelCount = 0;

                statisticsListBx.Items.Clear();

                try
                {
                   //Create reader and writers
                    StreamReader textFile = new StreamReader(openFileDialog1.FileName);
                    StreamWriter saveFile = File.CreateText("Statistics.txt");

                    while (!textFile.EndOfStream)
                    {
                        
                        readText = textFile.ReadLine();
                        readText = readText.ToLower();
                        
                        currentWord = readText.Split(' ');

                        //loop to analyze the current word
                        for (int i = 0; i < currentWord.Length; i++)
                        {
                            
                            currentVowelCount = 0;
                            wordCharArray = currentWord[i].ToCharArray();

                            //comparing string in currentWord for the first word
                            if (firstWordByAlphabet == "")
                            {
                                firstWordByAlphabet = currentWord[i];
                            }
                            else if (currentWord[i].CompareTo(firstWordByAlphabet) < 0)
                            {
                                firstWordByAlphabet = currentWord[i];
                            }

                            //comparing string in currentword for the last word
                            if (lastWordByAlphabet == "")
                            {
                                lastWordByAlphabet = currentWord[i];
                            }
                            else if (currentWord[i].CompareTo(lastWordByAlphabet) > 0)
                            {
                                lastWordByAlphabet = currentWord[i];
                            }

                            //looking for the longest word
                            if (longestWord == "")
                            {
                                longestWord = currentWord[i];
                            }
                            else if (longestWord.Length < currentWord[i].Length)
                            {
                                longestWord = currentWord[i];
                            }

                            //find any vowels within the word and count them
                            for (int j = 0; j < wordCharArray.Length; j++)
                            {
                                if (wordCharArray[j] == 'a' ||
                                wordCharArray[j] == 'e' ||
                                wordCharArray[j] == 'i' ||
                                wordCharArray[j] == 'o' ||
                                wordCharArray[j] == 'u')
                                {
                                    currentVowelCount++;
                                }
                            }

                            //Compare for the most vowels.
                            if (currentVowelCount > mostVowelCount)
                            {
                                mostVowelCount = currentVowelCount;
                                wordWithMostVowels = currentWord[i];
                            }
                        }
                    }

                    //Add items to the list box
                    statisticsListBx.Items.Add("First word alphabetically: " + firstWordByAlphabet);
                    statisticsListBx.Items.Add("Last word alphabetically: " + lastWordByAlphabet);
                    statisticsListBx.Items.Add("Longest word: " + longestWord + " at " + longestWord.Length + " characters.");
                    statisticsListBx.Items.Add("Word with the most vowels: " + wordWithMostVowels + " with " + mostVowelCount + " vowels");

                    //save to a file called statistics.txt it shows up in the debug folder of the project.
                    saveFile.WriteLine("First word alphabetically: " + firstWordByAlphabet);
                    saveFile.WriteLine("Last word alphabetically: " + lastWordByAlphabet);
                    saveFile.WriteLine("Longest word: " + longestWord + " at " + longestWord.Length + " characters.");
                    saveFile.WriteLine("Word with the most vowels: " + wordWithMostVowels + " with " + mostVowelCount + " vowels");

                    saveFile.Close();
                    textFile.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }//ends catch exception
            }//ends if dialog
        }//ends open file button

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }//ends class form
}//ends namespace
